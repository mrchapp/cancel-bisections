#!/bin/bash

PROJECT=Linaro/lkft/mirrors/stable/linux-stable-rc

# Define the $GITLAB_TOKEN
if [ -f api-gitlab.conf ]; then
  # shellcheck disable=SC1091
  source api-gitlab.conf
fi

if [ -z "${GITLAB_TOKEN}" ]; then
  echo "GITLAB_TOKEN has not been defined."
  exit 1
fi

if [ -t 0 ]; then
  # Running interactively
  if [ $# -gt 0 ]; then
    job_id="$1"
  fi
else
  # Running in a pipe or reading from a file
  read -r line
  job_id="$(echo "${line}" | awk '{print $1}')"
fi

if [ -z "${job_id}" ]; then
  echo "Need a job id to restart."
  exit 1
fi

CURL="curl --silent"
PRIV_CURL="${CURL} --header PRIVATE-TOKEN:${GITLAB_TOKEN}"

project_id="${PROJECT//\//%2F}"
project_url="https://gitlab.com/api/v4/projects/${project_id}"

# shellcheck disable=SC2068
${PRIV_CURL[@]} --request POST "${project_url}/jobs/${job_id}/retry"
