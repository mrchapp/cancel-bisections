#!/bin/bash

#PROJECT=mrchapp/linux
PROJECT=mrchapp/linux-developer

# Define the $GITLAB_TOKEN
if [ -f api-gitlab.conf ]; then
  # shellcheck disable=SC1091
  source api-gitlab.conf
fi

if [ -z "${GITLAB_TOKEN}" ]; then
  echo "GITLAB_TOKEN has not been defined."
  exit 1
fi

if [ -z "${TRIGGER_TOKEN}" ]; then
  echo "TRIGGER_TOKEN has not been defined."
  exit 1
fi

CURL="curl --silent"
#PRIV_CURL="${CURL} --header PRIVATE-TOKEN:${GITLAB_TOKEN}"

project_id="${PROJECT//\//%2F}"
project_url="https://gitlab.com/api/v4/projects/${project_id}"

show_help() {
  exit_code=${1:-0}
  echo ""
  echo "$0 <MANDATORY OPTIONS> [OTHER OPTIONS]"
  echo ""
  echo "Mandatory options:"
  echo "  --git-repo https://repo/url.git  Location of Git repo."
  echo "  --repo-name | -n name            Common name for the Git repo. Can be anything."
  echo "  --git-ref branch_or_tag          Branch or tag to use."
  echo ""
  echo "Other options:"
  echo "  --git-sha commit_id              Specific git commit ID."
  echo "  --patch-series|-p url_to_patches Additional (external) patches on top of the Git repo."
  exit "${exit_code}"
}

# From https://stackoverflow.com/a/9271406
process_args() {
  while [ "${1:-}" != "" ]; do
    case "$1" in
      "--git-ref")
        shift
        kernel_branch="$1"
        ;;
      "--git-repo")
        shift
        kernel_repo="$1"
        ;;
      "--git-sha")
        shift
        latest_sha="$1"
        ;;
      "--help" | "-h")
        show_help
        exit 0
        ;;
      "--patch-series" | "-p")
        shift
        patches="$1"
        ;;
      "--repo-name" | "-n")
        shift
        repo_name="$1"
        ;;
    esac
    shift
  done
}

kernel_branch=""
kernel_repo=""
latest_sha=""
patches=""
repo_name=""
process_args "$@"

if [ -z "${kernel_repo}" ]; then
  echo "ERROR: Kernel Git repository is required."
  show_help 1
fi

if [ -z "${repo_name}" ]; then
  echo "ERROR: Repository name is mandatory. (It's needed for QA Reports.)"
  show_help 1
fi

if [ -z "${kernel_branch}" ]; then
  echo "ERROR: Kernel branch is required. (It's needed for workflow rules and QA Reports.)"
  show_help 1
fi

# Try to resolve commit ID if only branch/tag is given
if [ -z "${latest_sha}" ] && [ -n "${kernel_branch}" ]; then
  # Try with tag first
  sha="$(git ls-remote "${kernel_repo}" "refs/tags/${kernel_branch}^{}" | awk '{print $1}')"
  if [ -z "${sha}" ]; then
    # Try with branch, then
    sha="$(git ls-remote "${kernel_repo}" "refs/heads/${kernel_branch}" | awk '{print $1}')"
  fi
  if [ -z "${sha}" ]; then
    echo "Could not resolve ${kernel_repo} Git reference to a Git commit."
    exit 1
  fi
  latest_sha="${sha}"
fi

# temporary files
pipeline_reply="$(mktemp)"

# shellcheck disable=SC2068
curl_cmd=("${CURL[*]}"
  --request POST
  --output "${pipeline_reply}"
  --form "ref=main"
  --form "token=${TRIGGER_TOKEN}"
  --form "variables[KERNEL_REPO]=${kernel_repo}"
  --form "variables[REPO_NAME]=${repo_name}"
  --form "variables[KERNEL_BRANCH]=${kernel_branch}"
  --form "variables[LATEST_SHA]=${latest_sha}"
)

if [ -n "${patches}" ]; then
  curl_cmd+=(--form "variables[PATCHES]=${patches}")
fi
curl_cmd+=("${project_url}/trigger/pipeline")

# shellcheck disable=SC2086
${curl_cmd[*]}

pipeline_url="$(jq -r '.web_url' "${pipeline_reply}")"

echo "Pipeline: ${pipeline_url}"

rm -f "${pipeline_reply}"
