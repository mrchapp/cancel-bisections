#!/bin/bash

PIPELINE_URL="$1"
if [ $# -lt 1 ]; then
  echo "Usage:"
  echo "  $0 <lkft pipeline url>"
  echo "Example:"
  echo "  $0 https://gitlab.com/Linaro/lkft/mirrors/stable/linux-stable-rc/-/pipelines/272287196"
  exit 1
fi
PROJECT="$(echo "${PIPELINE_URL}" | sed -e 's#^https://gitlab.com/##g' -e 's#/\-/pipelines/[0-9]*$##g')"

# Define the $GITLAB_TOKEN
if [ -f api-gitlab.conf ]; then
  # shellcheck disable=SC1091
  source api-gitlab.conf
fi

CURL="curl --silent"

project_id="${PROJECT//\//%2F}"
project_url="https://gitlab.com/api/v4/projects/${project_id}"

# Get latest pipeline
pipeline_id="${PIPELINE_URL##*/}"

# temporary files
jobs_json="$(mktemp)"
headers="$(mktemp)"

# ID's of manual jobs
:>"${jobs_json}"
page=1
while true; do
  #echo "Page ${page}..."
  # shellcheck disable=SC2068
  ${CURL[@]} -D "${headers}" "${project_url}/pipelines/${pipeline_id}/jobs?scope[]=failed&per_page=100&page=${page}" >> "${jobs_json}"
  if ! grep ^link: "${headers}" | grep -q next; then
    break
  fi
  page=$((page + 1))
done

while read -r job_id; do
  info="$(jq -rc ".[] | select(.id == ${job_id}) | .finished_at,.ref,.name,.commit.title" "${jobs_json}" | paste -sd"|" -)"
  finished_at="$(echo "${info}" | awk -F '|' '{print $1}')"
  ref="$(echo "${info}" | awk -F '|' '{print $2}')"
  name="$(echo "${info}" | awk -F '|' '{print $3}')"
  title="$(echo "${info}" | awk -F '|' '{print $4}')"

  dt_finished="$(date +"%Y-%m-%d %H:%M:%S" -d "${finished_at}")"

  echo -n  "${job_id}"
  echo -ne "\t"
  echo -n  "${dt_finished}"
  echo -ne "\t"
  echo -n  "${ref}"
  echo -ne "\t"
  echo -n  "${title}"
  echo -ne "\t"
  echo -n  "${name}"
  echo ""
#done < <(jq -r '.[] | .id' "${jobs_json}" | sort)
done < <(jq -r '.[] | select(.failure_reason == "runner_system_failure") | .id' "${jobs_json}" | sort)

rm -f "${jobs_json}" "${headers}"
