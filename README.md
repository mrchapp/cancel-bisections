# CLI tools for Gitlab

## Installation

Just git-clone, configure and use.

### Requirements

`peco` is a binary dependency. Can be installed directly with
`apt-get install peco`.

Mostly all scripts require a Gitlab API token, saved in
`api-gitlab.conf`:
```
GITLAB_TOKEN="1234abcd1234abcd"
```

Another token may be required for other scripts: `TRIGGER_TOKEN`:
```
TRIGGER_TOKEN="4321dcba4321dcba"
```
on the same `api-gitlab.conf` file. This is specific to the
Gitlab project to be triggered.


## Cancel bisections

This set of scripts will list currently running bisections.
Given that list, it will be possible to cancel each
individually.

### Usage

Just run the script:
```
./list-and-cancel.sh
```


## Retry Gitlab-runner failures

With this set of scripts, it's possible to look into the
pipelines and determine if any the jobs failed because of
gitlab-runner problems.

### Usage

Just run the script to fetch the most recent commits from the
linux-stable-rc tree and look for the related pipelines in
Gitlab:
```
./list-runner-failures.sh
```

A specific pipeline can be an argument to script in order to
look for failures there:
```
./list-runner-failures.sh https://gitlab.com/Linaro/lkft/mirrors/stable/linux-stable-rc/-/pipelines/513341365
```


## Start pipeline

These scripts will manage to create a new pipeline for any
Git tree, branch and commit ID, as specificed through the
command line:
```
./start-pipeline.sh \
  --git-repo https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git \
  --repo-name torvalds \
  --git-ref master \
  --git-sha 33b5bc9e703383e396f275d51fc4bafa48dbae5a
```

A patchset can be included too:
```
./start-pipeline.sh \
  --git-repo https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git \
  --repo-name torvalds \
  --git-ref master \
  --git-sha 33b5bc9e703383e396f275d51fc4bafa48dbae5a \
  --patch-series https://lore.kernel.org/linux-kselftest/20220325232709.2358965-1-kuba@kernel.org/
```
