#!/bin/bash

root_dir="$(dirname "$(readlink -e "$0")")"

if ! command -v peco > /dev/null; then
  echo "This requires peco. Please install before running."
  exit 1
fi

pipelines="$(mktemp)"
"${root_dir}/api-listbisections.sh" > "${pipelines}"

if [ -s "${pipelines}" ]; then
  peco --prompt "Filter>" --exec "${root_dir}/cancel-pipeline.sh" "${pipelines}"
else
  echo "No bisection pipelines are currently running."
fi

rm "${pipelines}"
