#!/bin/bash

PROJECT=Linaro/lkft/bisect

# Define the $GITLAB_TOKEN
if [ -f api-gitlab.conf ]; then
  # shellcheck disable=SC1091
  source api-gitlab.conf
fi

if [ -z "${GITLAB_TOKEN}" ]; then
  echo "GITLAB_TOKEN has not been defined."
  exit 1
fi

read -r line
pipeline_id="$(echo "${line}" | awk '{print $1}')"


CURL="curl --silent"
PRIV_CURL="${CURL} --header PRIVATE-TOKEN:${GITLAB_TOKEN}"

project_id="${PROJECT//\//%2F}"
project_url="https://gitlab.com/api/v4/projects/${project_id}"

# shellcheck disable=SC2068
${PRIV_CURL[@]} --request POST "${project_url}/pipelines/${pipeline_id}/cancel"
