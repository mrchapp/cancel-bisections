#!/bin/bash

PROJECT=Linaro/lkft/mirrors/stable/linux-stable-rc
LINUX_STABLE_RC_REPO=https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable-rc.git
BRANCHES=(4.9 4.14 4.19 5.4 5.10 5.15 5.16 5.17)

# Define the $GITLAB_TOKEN
# shellcheck disable=SC1091
source api-gitlab.conf

CURL="curl --silent"
ROOT_DIR="$(dirname "$(readlink -e "$0")")"

project_id="${PROJECT//\//%2F}"
project_url="https://gitlab.com/api/v4/projects/${project_id}"

# temporary files
runner_failures="$(mktemp)"

if [ $# -eq 0 ]; then
  # Get all pipelines
  temp_pipelines="$(mktemp)"
  # shellcheck disable=SC2068
  ${CURL[@]} "${project_url}/pipelines" > "${temp_pipelines}"

  for branch in ${BRANCHES[*]}; do
    #commit="$(git -C "${KERNEL_DIR}" rev-parse "${KERNEL_REMOTE_NAME}/linux-${branch}.y")"
    commit="$(git ls-remote "${LINUX_STABLE_RC_REPO}" "refs/heads/linux-${branch}.y" | awk '{print $1}')"

    # Find pipeline for specific commit-id
    pipeline_id="$(jq -r ".[] | select(.sha==\"${commit}\").id" "${temp_pipelines}")"

    if [ -z "${pipeline_id}" ]; then
      echo "${branch}: Pipeline not found for given commit ${commit}" >&2
      continue
    fi

    "${ROOT_DIR}/api-listrunnerfailures.sh" "${PROJECT}/-/pipelines/${pipeline_id}"
  done | sort -k2 > "${runner_failures}"
else
  "${ROOT_DIR}/api-listrunnerfailures.sh" "$1" | sort -k2 > "${runner_failures}"
fi

echo "Runner failures: ${runner_failures}"

if [ -s "${runner_failures}" ]; then
  peco --prompt "Filter>" --exec "${ROOT_DIR}/restart-job.sh" "${runner_failures}"
else
  echo "No jobs failed due to Gitlab runner problems."
fi

rm -f "${temp_pipelines}"
