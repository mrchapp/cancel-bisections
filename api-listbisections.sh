#!/bin/bash

PROJECT=Linaro/lkft/bisect

# Define the $GITLAB_TOKEN
if [ -f api-gitlab.conf ]; then
  # shellcheck disable=SC1091
  source api-gitlab.conf
fi

if [ -z "${GITLAB_TOKEN}" ]; then
  echo "GITLAB_TOKEN has not been defined."
  exit 1
fi

status_to_icon() {
  if [ ! $# -eq 1 ]; then
    return
  fi

  status=$1
  case ${status} in
    success)  icon="o" ;;
    failed)   icon="x" ;;
    pending)  icon="#" ;;
    created)  icon=" " ;;
    running)  icon="-" ;;
    *)        icon="?"; echo "${status}" >&2 ;;
  esac

  echo "${icon}"
}

CURL="curl --silent"
PRIV_CURL="${CURL} --header PRIVATE-TOKEN:${GITLAB_TOKEN}"

project_id="${PROJECT//\//%2F}"
project_url="https://gitlab.com/api/v4/projects/${project_id}"

# Temporary files
pipelines_running_json="$(mktemp)"
pipelines_pending_json="$(mktemp)"
pipeline_jobs="$(mktemp)"
pipeline_vars="$(mktemp)"
status_json="$(mktemp)"

# Get pipelines running or pending
# shellcheck disable=SC2068
${CURL[@]} "${project_url}/pipelines?status=running" > "${pipelines_running_json}"
# shellcheck disable=SC2068
${CURL[@]} "${project_url}/pipelines?status=pending" > "${pipelines_pending_json}"
#${CURL[@]} "${project_url}/pipelines?status=success" > pipelines.json

declare -a pipelines
# shellcheck disable=SC2207
pipelines=($(jq '.[].id' <(cat "${pipelines_running_json}" "${pipelines_pending_json}") | sort -n))
echo "Pipelines are: [${pipelines[*]}]" >&2

# shellcheck disable=SC2068
for pipeline_id in ${pipelines[@]}; do
  echo "Fetching info for pipeline ${pipeline_id}..." >&2

  # Get pipeline's variables
  # shellcheck disable=SC2068
  ${PRIV_CURL[@]} "${project_url}/pipelines/${pipeline_id}/variables" > "${pipeline_vars}"

  # Get Tuxbuild status.json
  tuxbuild_url="$(jq -r '.[] | select(.key == "NEW_BUILD_URL") | .value' "${pipeline_vars}")"
  if [ -z "${tuxbuild_url}" ]; then
    continue
  fi
  # shellcheck disable=SC2068
  ${CURL[@]} -sSL --fail "${tuxbuild_url}/status.json" > "${status_json}"
  git_repo="$(jq -r '.git_repo' "${status_json}")"
  git_describe="$(jq -r '.git_describe' "${status_json}")"
  target_arch="$(jq -r '.target_arch' "${status_json}")"
  toolchain="$(jq -r '.toolchain' "${status_json}")"
  targets="$(jq -r '.targets | join(",")' "${status_json}")"

  # Get each job's status
  # shellcheck disable=SC2068
  ${CURL[@]} "${project_url}/pipelines/${pipeline_id}/jobs" > "${pipeline_jobs}"
  declare -A jobs
  while read -r line; do
    job="$(echo "${line}" | cut -d, -f1)"
    status="$(echo "${line}" | cut -d, -f2)"
    jobs[${job}]="${status}"
  done < <(jq -r '.[] | (.name,.status)' "${pipeline_jobs}" | pr -aTt -s, -2)

  # Bar with jobs' status
  pipeline_status=""
  for job in find_old bisect revert report; do
    pipeline_status="${pipeline_status}[$(status_to_icon "${jobs[${job}]}")]"
  done

  printf "%d %s %9s %8s   %30s   %s   %s\n" "${pipeline_id}" "${pipeline_status}" "${target_arch}" "${toolchain}" "${git_describe}" "${git_repo}" "${targets}"
done

# Clean up
rm "${pipelines_running_json}" "${pipelines_pending_json}" "${pipeline_jobs}" "${pipeline_vars}" "${status_json}"
